package main

import (
	"gitlab.com/amarjeetr/test-monorepo-one/svc/a/internal"
	"log"
)

func main() {
	s := internal.New("8080")
	log.Println("starting server on port 8080")
	if err := s.ListenAndServe(); err != nil {
		panic(err)
	}
}
