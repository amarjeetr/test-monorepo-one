package internal

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	test_module "gitlab.com/amarjeetr/test-module"
	"gitlab.com/amarjeetr/test-monorepo-one/pkg"
	"net/http"
)

func New(port string) http.Server {
	r := chi.NewRouter()

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		out := fmt.Sprintf("service a, %d, %d\n", pkg.Sum(5, 6), pkg.Sum3(5, 6, 7))
		_, _ = w.Write([]byte(out))

		out = fmt.Sprintf("service a, %d\n", test_module.Sub3(5, 2, 1))
		_, _ = w.Write([]byte(out))
	})

	return http.Server{
		Addr:    ":" + port,
		Handler: r,
	}
}
