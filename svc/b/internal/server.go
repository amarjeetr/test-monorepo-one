package internal

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"gitlab.com/amarjeetr/test-module"
	test_module_v2 "gitlab.com/amarjeetr/test-module/v2"
	"gitlab.com/amarjeetr/test-monorepo-one/pkg"
	"net/http"
)

func New(port string) http.Server {
	r := chi.NewRouter()

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		out := fmt.Sprintf("service b, %d\n", pkg.Sum(1, 2))
		_, _ = w.Write([]byte(out))

		out = fmt.Sprintf("service b, %d\n", test_module.Multiply(1, 2))
		_, _ = w.Write([]byte(out))

		out = fmt.Sprintf("service b, %d\n", test_module_v2.SubMany(9, 4, 2))
		_, _ = w.Write([]byte(out))

		out = fmt.Sprintf("service b, %d\n", test_module.Sub(5, 2))
		_, _ = w.Write([]byte(out))
	})

	return http.Server{
		Addr:    ":" + port,
		Handler: r,
	}
}
