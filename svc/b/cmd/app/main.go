package main

import (
	"gitlab.com/amarjeetr/test-monorepo-one/svc/b/internal"
	"log"
)

func main() {
	s := internal.New("8081")
	log.Println("starting server on port 8081")
	if err := s.ListenAndServe(); err != nil {
		panic(err)
	}
}
