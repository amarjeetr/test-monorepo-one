module gitlab.com/amarjeetr/test-monorepo-one

go 1.20

require github.com/go-chi/chi/v5 v5.0.8

require (
	gitlab.com/amarjeetr/test-module v1.1.0
	gitlab.com/amarjeetr/test-module/v2 v2.0.0
)
